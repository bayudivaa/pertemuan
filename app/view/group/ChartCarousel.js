/**
 * Demonstrates how to use an Ext.Carousel in vertical and horizontal configurations
 */
Ext.define('Pertemuan.view.group.ChartCarousel', {
    extend: 'Ext.Container',
    xtype: 'chartcarousel',

    requires: [
        'Ext.carousel.Carousel',
        'Pertemuan.view.chart.PieChart',
    ],

    shadow: true,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
   /** defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },*/ 
    items: [{
        xtype: 'carousel',
        width:'100%',
        height: '100%',
        items: [{
            xtype: 'grid-column-stacked',
            
        },
        {
            xtype: 'piechart',
            
        },
        {
            xtype: 'radarchart',
            
        }]
    },]
});