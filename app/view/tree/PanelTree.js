Ext.define('Pertemuan.view.tree.PanelTree', {
    extend: 'Ext.Container',
    xtype: 'treepannel',
    requires: [
        'Ext.layout.HBox',
        'Pertemuan.view.tree.Treelist',
    ],
    layout: {
        type: 'hbox',
        pack: 'center',
        align: 'stretch'
    },
    margin: '5',
    defaults:{
        
        bodyPadding: 5
    },
    items: [
        {
            xtype: 'tree-list',
            flex: 0.35   
        },
        {
            xtype: 'panel',
            id: 'detaillist',
            flex: 1,
            scrollable: 'y',
            xtype: 'datatree'
        }
    ]
});