Ext.define('Pertemuan.view.tree.Treelist', {
    extend: 'Ext.grid.Tree',
    xtype: 'tree-list',
    requires: [
        'Ext.grid.plugin.MultiSelection'
    ],

    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{navItems}',
    listeners: {
        
        itemTap: function (me, index, target, record, eOpts){
            detaillist = Ext.getCmp('detaillist');
            personnelStore = Ext.getStore('personnel');
            var rec= (record.data.text);
           
            if(rec == "Games"){
                var pilih = "Games"
                personnelStore.filter('hobi', pilih);
            }
            else if(rec == "Olahraga"){
                var pilih = "Olahraga"
                personnelStore.filter('hobi', pilih);
            }
            else if(rec == "Traveling"){
                var pilih = "Traveling"
                personnelStore.filter('hobi', pilih);
            }
            else if(rec == "Shoping"){
                var pilih = "Shoping"
                personnelStore.filter('hobi', pilih);
            }
            
        }
    }
});