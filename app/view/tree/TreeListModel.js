Ext.define('KitchenSink.view.grid.TreeListModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.tree-list',

    formulas: {
        selectionText: function(get) {
            var selection = get('treelist.selection'),
                path;
            if (selection) {
                path = selection.getPath('text');
                path = path.replace(/^\/Root/, '');
                return 'Selected: ' + path;
            } else {
                return 'No node selected';
            }
        }
    }, 

    stores: {
        navItems: {
            type: 'tree',
            rootVisible: true,
            root: {
                expanded: true,
                text: 'All',
                iconCls: 'x-fa fa-sitemap',
                children: [{
                    text: 'Hobi',
                    iconCls: 'x-fa fa-user',
                    children: [{
                        text: 'Games',
                        iconCls: 'x-fa fa-gamepad',
                        leaf: true
                    }, {
                        text: 'Shoping',
                        iconCls: 'x-fa fa-inbox',
                        leaf: true
                    },{
                        text: 'Olahraga',
                        iconCls: 'x-fa fa-star',
                        leaf: true
                    },{
                        text: 'Traveling',
                        iconCls: 'x-fa fa-tag',
                        leaf: true
                    }]
                }]
            }
        }
    }
});